PREREQUISITES
=============

To install Wire, you need the following prerequisites:

    1. OpenSSL Library
       http://www.openssl.org/source/

    2. GNU libiconv
       http://www.gnu.org/software/libiconv/

    3. GNU readline
       http://cnswww.cns.cwru.edu/php/chet/readline/rltop.html

These are usually distributed with operating systems.

If compiling from the Git repository, GNU Autotools are also required.

INSTALLATION
============

For example to install it up to Debian 8 (Jessie):

	sudo apt-get install build-essential git libssl-dev libiconv-hook-dev libreadline-dev libxml2-dev zlib1g-dev

These are usually distributed with operating systems.

### Important for Debian 9 (Stretch) an higher Users
To install it (amd64 in this example):

	sudo apt-get install build-essential git libiconv-hook-dev libreadline-dev libxml2-dev
	wget http://security.debian.org/debian-security/pool/updates/main/o/openssl1.0/libssl1.0-dev_1.0.2l-2+deb9u3_amd64.deb
	sudo dpkg -i libssl1.0-dev_1.0.2l-2+deb9u3_amd64.deb
	sudo apt --fix-broken install
	sudo apt-get install zlib1g-dev

By installing the libssl1.0-dev a dependency error occurs. Simply ignore it and execute the 5 lines above normally.

To install Wire, follow these instructions:

    git clone https://github.com/ProfDrLuigi/wire.git wire

    cd wire

    ./configure

  This will install Wire into /usr/local/bin by default. To
  change this, instead run:

    ./configure --prefix=PATH

  3. Run make. Note, GNU make is required.

     gmake

    4. If make is successful, install the software:

         gmake install

       This will require write permissions to /usr/local/bin, or
       whatever directory you set as the prefix above.


RUNNING
=======

To start Wire, simply run:

    wire
    
    
    
    
NOTE ABOUT MAC OS X 10.7
===========================

The current version of GNU Readline (6.4) fails to compile on MAC OS X 10.7. 
To compile it succesfuly, you can apply the following steps:

export MACOSX_DEPLOYMENT_TARGET=10.7
export CFLAGS="-arch x86_64 -g -Os -pipe -no-cpp-precomp"
export CCFLAGS="-arch x86_64 -g -Os -pipe"
export CXXFLAGS="-arch x86_64 -g -Os -pipe"
export LDFLAGS="-arch x86_64 -bind_at_load"

LIBS="-liconv" LDFLAGS="-L/usr/local/Cellar/libiconv/1.14/lib" CPPFLAGS="-I/usr/local/Cellar/libiconv/1.14/include" ./configure --enable-pthreads



Install readline 6.2 on Lion and higher:
----------------------------------------

cd readline-6.2-src/

./configure

cd shlib
 
sed -e 's/-dynamic/-dynamiclib/' Makefile > Makefile.good
 
mv Makefile.good Makefile
 
cd ..
 
make
 
sudo make install


